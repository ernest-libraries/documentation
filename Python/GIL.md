# Global Interpreter Lock (GIL)

## Definition

**A mutex (or a lock) that allows only one thread to hold the control of the Python Interpreter.**

**A single lock on the interpreter itself which adds a rule that execution of any Python bytecode requires acquiring the interpreter lock.**

* only one thread can be in a state of execution at any point in time

* a performance bottleneck in CPU-bound and multi-threaded code



### GIL as a solution (Thread-safe memory management)

Python uses ***reference counting*** for <u>memory management</u>. 

* Objects created in Python have a **<u>reference count variable</u>** that **keeps track of the number of references that point to the object**. 
* When this count reaches zero, the memory occupied by the object is released.



##### Example:

```python
import sys
a = []
b = a
sys.getrefcount(a)	# It is 3.
```

In the above example, the reference count for the empty list object [] was 3. The list object was referenced by a, b and the argument passed to sys.getrefcount().

The problem was that this **<u>reference count variable</u>** needed protection from ***race conditions*** where <u>two threads increase or decrease its value simultaneously</u>. If this happens, it can cause either leaked memory that is never released or, even worse, incorrectly release the memory while a reference to that object still exists. This can cause crashes or other “weird” bugs in your Python programs.

This reference count variable can be kept safe by **adding locks to all data structures** that are shared across threads so that they are not modified inconsistently.



* <u>Prevents deadlocks</u> (as there is only one lock) and <u>doesn’t introduce much performance overhead</u>. But it effectively makes any CPU-bound Python program single-threaded.
* In multi-threaded version, <u>the GIL prevented the ***CPU-bound*** threads from executing in parallel</u>.
* The GIL <u>does NOT have much impact on the performance of ***I/O-bound*** multi-threaded programs</u> as the lock is shared between threads while they are waiting for I/O.



### The Impact on Multi-Threaded Python Programs

***CPU-bound*** programs: those that are <u>pushing the CPU to its limit</u>. This includes programs that do mathematical computations like matrix multiplications, searching, image processing, etc.

***I/O-bound*** programs: the ones that <u>spend time waiting for Input/Output which can come from a user, file, database, network, etc</u>. I/O-bound programs sometimes have to wait for a significant amount of time till they get what they need from the source due to the fact that the source may need to do its own processing before the input/output is ready, for example, a user thinking about what to enter into an input prompt or a database query running in its own process.



##### Example:

Simple CPU-bound program that performs a countdown:

```python
# single_threaded.py
import time
from threading import Thread

COUNT = 50000000

def countdown(n):
    while n>0:
        n -= 1

start = time.time()
countdown(COUNT)
end = time.time()

print('Time taken in seconds -', end - start)
```

```bash
$ python single_threaded.py
Time taken in seconds - 6.20024037361145
```

The same countdown using two threads in parallel:

```python
# multi_threaded.py
import time
from threading import Thread

COUNT = 50000000

def countdown(n):
    while n>0:
        n -= 1

t1 = Thread(target=countdown, args=(COUNT//2,))
t2 = Thread(target=countdown, args=(COUNT//2,))

start = time.time()
t1.start()
t2.start()
t1.join()
t2.join()
end = time.time()

print('Time taken in seconds -', end - start)
```

```bash
$ python multi_threaded.py
Time taken in seconds - 6.924342632293701
```

Both versions take almost same amount of time to finish. In the multi-threaded version the GIL prevented the CPU-bound threads from executing in parallel.

The GIL does not have much impact on the performance of I/O-bound multi-threaded programs as the lock is shared between threads while they are waiting for I/O.

But a program whose threads are entirely CPU-bound, e.g., a program that processes an image in parts using threads, would not only become single threaded due to the lock but will also see an increase in execution time, as seen in the above example, in comparison to a scenario where it was written to be entirely single-threaded.



### Python 3

* Python's GIL was known to starve the I/O bound threads by not giving them a chance to acquire the GIL from CPU-bound threads.

* force threads to release the GIL after a fixed interval of continuous use and if nobody else acquired the GIL, the same thread could continue its use.

#### Solution: Multi-processing V.S. Multi-threading

* Multi-processing: Each Python process gets it own Python interpreter and memory space.



<div style="page-break-after: always;"></div>

## Take-away

* **If not for CPU intensive tasks, Python threads are helpful in dealing with blocking I/O operations** including reading & writing files, interacting with networks, communicating with devices like displays, .... These tasks happen when Python make certain types of system calls. Tasks that spend much of their time waiting for external events are generally good candidates for threading.



<div style="page-break-after: always;"></div>

## Reference

* ##### What Is the Python Global Interpreter Lock (GIL)? 

  Link: https://realpython.com/python-gil/

* ##### What is the Python Global Interpreter Lock (GIL) 

  Link: https://www.geeksforgeeks.org/what-is-the-python-global-interpreter-lock-gil/

* ##### GlobalInterpreterLock 

  Link: https://wiki.python.org/moin/GlobalInterpreterLock

* ##### Python’s GIL — A Hurdle to Multithreaded Program

  Link: https://medium.com/python-features/pythons-gil-a-hurdle-to-multithreaded-program-d04ad9c1a63